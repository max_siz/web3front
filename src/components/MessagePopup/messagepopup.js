import React from 'react';

import {
} from '../../reducers'

class MessagePopup extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			html: '',
		};
	}

	componentDidMount() {
		this.unsubscribe = this.props.store.subscribe(() => {
			this.setState({
				html: this.props.store.getState()._messagePopup,
			});
		})
	}
	componentWillUnmount() {
		this.unsubscribe();
	}


	render() {

		return (
			<div className="modal" id="modal-1">
				<div className="modal__inner">
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">

							{ this.state.html }

						</div>
					</div>
				</div>
			</div>
		)

	}
}

export default MessagePopup;