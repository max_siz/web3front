import React from 'react';

import dai_icon  from '../../static/pics/currency/dai.svg'
import usdc_icon from '../../static/pics/currency/usdc.svg'
import usst_icon from '../../static/pics/currency/usst.svg'
import tusd_icon from '../../static/pics/currency/tusd.svg'

class CurrencyReservesPage extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
	}
	componentWillUnmount() {
		// this.unsubscribe();
	}


	render() {

		return (
			<div className="content__block">
				<div className="cb-heading">
					<h3>Currency Reserves</h3>
				</div>
				<div className="cb-content">
					<div className="block-cres">
						<div className="row-big">
							<div className="col">
								<div className="cres-item">
									<img src={ dai_icon } alt="" />
									<span>DAI:</span><span>3,023,553.23</span>
									<span className="value">(21.09%)</span>
								</div>
								<div className="cres-item">
									<img src={ usdc_icon } alt="" />
									<span>USDC:</span><span>3,423,553.88</span>
									<span className="value">(21.09%)</span>
								</div>
							</div>
							<div className="col">
								<div className="cres-item">
									<img src={ usst_icon } alt="" />
									<span>DAI:</span><span>3,023,553.23</span>
									<span className="value">(21.09%)</span>
								</div>
								<div className="cres-item">
									<img src={ tusd_icon } alt="" />
									<span>USDC:</span><span>3,423,553.88</span>
									<span className="value">(21.09%)</span>
								</div>
							</div>
						</div>
						<div className="cres-total">
							<div className="sum">
								<span><img src={ dai_icon } alt="" />DAI</span>
								<span><img src={ usdc_icon } alt="" />USDC</span>
								<span><img src={ usst_icon } alt="" />USDC</span>
								<span><img src={ tusd_icon } alt="" />TUSD</span></div>
							<div className="total">3,630,206.23</div>
						</div>
						<div className="row-big">
							<div className="col">
								<div className="cres-det"> <span>Daily USD Vol:</span><span className="value">$1,324,443,70</span></div>
								<div className="cres-det"> <span>FEE:</span><span className="value">0.030%</span></div>
								<div className="cres-det"> <span>Admin FEE:</span><span className="value">33.12%</span></div>
							</div>
							<div className="col">
								<div className="with-tip">
									<div className="cres-det"> <span>Virtual Price:</span><span className="value">1.0321</span></div>
									<button className="i-tip" data-tippy-content="Vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas"></button>
								</div>
								<div className="cres-det"> <span>A:</span><span className="value">100</span></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)

	}
}

export default CurrencyReservesPage;