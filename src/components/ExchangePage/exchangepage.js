import React from 'react';

import CurrencyReservesPage from '../CurrencyReservesPage'
import PoolsPage from '../PoolsPage'

class ExchangePage extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
	}
	componentWillUnmount() {
		// this.unsubscribe();
	}


	render() {

		return (
			<React.Fragment>
				<h1>Exchange</h1>
				<div className="content__block cb-exchange">
					<div className="cb-content">
						<div className="exchange-box">
							<div className="row-big">
								<div className="col">
									<div className="control-wrap">
										<label className="label">From:</label>
										<div className="cw-select">
											<input className="form-control" type="text" value="22.00" placeholder="0.00" />
											<div className="select"></div>
										</div>
										<span className="note">MAX: 3 232.23</span>
									</div>
								</div>
								<button className="btn-exchange">
									<svg width="28" height="22" viewBox="0 0 28 22" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M21.7778 4.66667L10.8889 4.66667L10.8889 7.77778L21.7778 7.77778V12.4444L28 6.22222L21.7778 0V4.66667ZM0 15.5556L6.22222 21.7778V17.1111H17.1111V14H6.22222V9.33333L0 15.5556Z" fill="#ECC54A"></path>
									</svg>
								</button>
								<div className="col">
									<div className="control-wrap cw-select">
										<label className="label">To:</label>
										<div className="cw-select">
											<input className="form-control" type="text" value="22.00" placeholder="0.00" />
											<div className="select"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="exchange-rate"> <span>Exchange Rate</span>
							<button className="btn btn-icon">
								<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M19.9619 14.2862C19.8296 13.879 19.3922 13.6564 18.985 13.7884L17.9768 14.116C18.5944 12.8388 18.9178 11.4342 18.9178 10C18.9178 4.78435 14.6747 0.541107 9.45892 0.541107C4.24324 0.541107 0 4.78435 0 10C0 15.2157 4.24324 19.4589 9.45892 19.4589C9.88708 19.4589 10.2342 19.1118 10.2342 18.6836C10.2342 18.2555 9.88708 17.9083 9.45892 17.9083C5.09826 17.9083 1.55064 14.3607 1.55064 10C1.55064 5.63937 5.09826 2.09175 9.45892 2.09175C13.8196 2.09175 17.3672 5.63937 17.3672 10C17.3672 11.2016 17.0961 12.3779 16.578 13.4467L16.2336 12.3863C16.1013 11.9791 15.6638 11.7563 15.2567 11.8885C14.8494 12.0209 14.6265 12.4583 14.7588 12.8655L15.6731 15.6795C15.7795 16.0072 16.0834 16.2155 16.4103 16.2155C16.4896 16.2155 16.5704 16.2032 16.65 16.1774L19.464 15.2633C19.8712 15.1309 20.0942 14.6936 19.9619 14.2862Z"></path>
								</svg>
							</button><span>USDC/DAI:</span><span className="rate">1.0031</span>
						</div>
						<div className="exchange-info">Infinite Approval - Trust This Contract Forever </div>
						<div className="exchange-esc">Estimated TX Cost: <span>24.86$</span></div>
						<div className="btns-block">
							<div className="row-big">
								<div className="col">
									<button className="btn btn-big btn-border btn-toggle">Advanced Options
										<svg width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M3.22855 0.935338C3.62853 0.450385 4.37147 0.450385 4.77145 0.935338L6.98054 3.61371C7.5185 4.26595 7.05456 5.25 6.20909 5.25L1.79091 5.25C0.945445 5.25 0.481499 4.26596 1.01946 3.61372L3.22855 0.935338Z" fill="#ECC54A"></path>
										</svg>
									</button>
								</div>
								<div className="col">
									<button className="btn btn-big btn-active">Sell</button>
								</div>
							</div>
						</div>
						<div className="options-block">
							<div className="ob-radios">
								<div className="row-big">
									<div className="col">
										<h4>MAX Slippage:</h4>
										<div className="row-small">
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="slippage" value="0.5%" checked="" />
													<span className="check">0.5%</span>
												</label>
											</div>
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="slippage" value="1%" />
													<span className="check">1%</span>
												</label>
											</div>
											<div className="col">
												<div className="cw-percent">
													<input className="form-control fc-small" type="text" placeholder="0" />
												</div>
											</div>
										</div>
									</div>
									<div className="col">
										<h4>Gas Price:</h4>
										<div className="row-small">
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="gas" value="120" checked="" />
													<span className="check">120 Standard</span>
												</label>
											</div>
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="gas" value="129" />
													<span className="check">129 Fast </span>
												</label>
											</div>
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="gas" value="180" />
													<span className="check">180 Instant </span>
												</label>
											</div>
											<div className="col">
												<input className="form-control fc-small text-center" type="text" placeholder="0.00" value="108.09" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<CurrencyReservesPage store = { this.props.store } />
				<PoolsPage store = { this.props.store } />
			</React.Fragment>
		)

	}
}

export default ExchangePage;