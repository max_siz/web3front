import React from 'react';

import CurrencyReservesPage from '../CurrencyReservesPage';

import dai_icon  from '../../static/pics/currency/dai.svg'
import usdc_icon from '../../static/pics/currency/usdc.svg'
import usst_icon from '../../static/pics/currency/usst.svg'
import tusd_icon from '../../static/pics/currency/tusd.svg'

class DepositPage extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
	}
	componentWillUnmount() {
		// this.unsubscribe();
	}


	render() {

		return (
			<React.Fragment>
				<h1>Deposit</h1>
				<div className="content__block cb-currency">
					<div className="cb-heading">
						<h3>Currencies</h3>
					</div>
					<div className="cb-content">
						<div className="control-wrap">
							<label className="currency-label">
								<img src={ dai_icon } alt="" />
								<span className="cur">DAI MAX:</span>
								<span className="sum">3,023,553.23</span>
							</label>
							<input className="form-control" type="text" value="22.00" />
						</div>
						<div className="control-wrap">
							<label className="currency-label">
								<img src={ usdc_icon } alt="" />
								<span className="cur">USDC MAX:</span>
								<span className="sum">0.00</span>
							</label>
							<input className="form-control" type="text" value="0.00" />
						</div>
						<div className="control-wrap">
							<label className="currency-label">
								<img src={ usst_icon } alt="" />
								<span className="cur">USDT MAX:</span>
								<span className="sum">0.00</span>
							</label>
							<input className="form-control" type="text" value="0.00" />
						</div>
						<div className="control-wrap">
							<label className="currency-label">
								<img src={ tusd_icon } alt="" />
								<span className="cur">TUSD MAX:</span>
								<span className="sum">0.00</span>
							</label>
							<input className="form-control" type="text" value="0.00" />
						</div>
						<div className="checkbox-block">
							<div className="control-wrap">
								<label className="checkbox">
									<input type="checkbox" />
									<span className="check"></span>
									<span className="text">Use maximum amount of coins available</span>
								</label>
							</div>
							<div className="control-wrap">
								<div className="with-tip">
									<label className="checkbox">
										<input type="checkbox" />
										<span className="check"></span>
										<span className="text">Infinite approval - trust this contrast forever </span>
									</label>
									<button className="i-tip" data-tippy-content="Vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas"></button>
								</div>
							</div>
						</div>
						<div className="btns-block">
							<div className="row-big">
								<div className="col">
									<button className="btn btn-big btn-border btn-toggle">ADVANCED OPTIONS
										<svg width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M3.22855 0.935338C3.62853 0.450385 4.37147 0.450385 4.77145 0.935338L6.98054 3.61371C7.5185 4.26595 7.05456 5.25 6.20909 5.25L1.79091 5.25C0.945445 5.25 0.481499 4.26596 1.01946 3.61372L3.22855 0.935338Z" fill="#ECC54A"></path>
										</svg>
									</button>
								</div>
								<div className="col">
									<button className="btn btn-big btn-active">DEPOSIT</button>
								</div>
							</div>
						</div>
						<div className="options-block">
							<div className="ob-radios">
								<div className="row-big">
									<div className="col">
										<h4>MAX Slippage:</h4>
										<div className="row-small">
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="slippage" value="0.5%" checked="" />
													<span className="check">0.5%</span>
												</label>
											</div>
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="slippage" value="1%" />
													<span className="check">1%</span>
												</label>
											</div>
											<div className="col">
												<div className="cw-percent">
													<input className="form-control fc-small" type="text" placeholder="0" />
												</div>
											</div>
										</div>
									</div>
									<div className="col">
										<h4>Gas Price:</h4>
										<div className="row-small">
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="gas" value="120" checked="" />
													<span className="check">120 Standard</span>
												</label>
											</div>
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="gas" value="129" />
													<span className="check">129 Fast </span>
												</label>
											</div>
											<div className="col">
												<label className="btn-radio">
													<input type="radio" name="gas" value="180" />
													<span className="check">180 Instant </span>
												</label>
											</div>
											<div className="col">
												<input className="form-control fc-small text-center" type="text" placeholder="0.00" value="108.09" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<input className="form-control fc-mid" type="text" value="Slippage (Plus Pricing): 0.000%" disabled="" />
						</div>
					</div>
				</div>

				<CurrencyReservesPage store = { this.props.store } />
			</React.Fragment>
		)

	}
}

export default DepositPage;