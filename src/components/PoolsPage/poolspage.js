import React from 'react';

import dai_icon   from '../../static/pics/currency/dai.svg'
import usdc_icon  from '../../static/pics/currency/usdc.svg'
import usst_icon  from '../../static/pics/currency/usst.svg'
import hbtc_icon  from '../../static/pics/currency/hbtc.svg'
import wbtc_icon  from '../../static/pics/currency/wbtc.svg'
import arrow_icon from '../../static/pics/arrow.svg'

class PoolsPage extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
	}
	componentWillUnmount() {
		// this.unsubscribe();
	}


	render() {

		return (
			<div className="content__block">
				<div className="cb-heading">
					<h3>Pools</h3>
				</div>
				<div className="cb-content">
					<table className="tbl-pool">
						<thead>
							<tr>
								<th>Pool</th>
								<th>Reward APY</th>
								<th>Volume
									<svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path className="top" d="M4.77145 15.0647C4.37147 15.5496 3.62853 15.5496 3.22855 15.0647L1.01946 12.3863C0.481499 11.734 0.945444 10.75 1.79091 10.75L6.20909 10.75C7.05456 10.75 7.5185 11.734 6.98054 12.3863L4.77145 15.0647Z" fill="#CDCDCD"></path>
										<path className="down" d="M3.61427 1.25348C3.81426 1.011 4.18574 1.011 4.38573 1.25348L6.59482 3.93186C6.86379 4.25798 6.63182 4.75 6.20909 4.75L1.79091 4.75C1.36818 4.75 1.13621 4.25798 1.40518 3.93186L3.61427 1.25348Z" fill="#ECC54A" stroke="#ECC54A"></path>
									</svg>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td className="pool-pl">
									<span className="icons">
										<img src={ dai_icon } alt="DAI" />
										<img src={ wbtc_icon } alt="WBTC" />
										<img src={ usdc_icon } alt="USDC" />
										<img src={ usst_icon } alt="USST" />
									</span>
									<span className="heading">3 Pools<i>USD</i><span>DAI + USDC + USDT</span></span>
								</td>
								<td className="pool-rew">
									<span className="thead-mob">Reward APY</span>
									<span className="percent">
										<span>+7.72%</span>
										<img src={ arrow_icon } alt="arrow" />
										<span>+7.72%</span></span><span className="title">Curve</span>
								</td>
								<td className="pool-vl"> <span className="thead-mob">Volume</span>$62,3m</td>
							</tr>
							<tr>
								<td className="pool-pl">
									<span className="icons">
										<img src={ hbtc_icon } alt="hBTC" />
										<img src={ wbtc_icon } alt="wBTC" />
									</span>
									<span className="heading">hBTC<i>BTC</i><span>hBTC + wBTC</span></span>
								</td>
								<td className="pool-rew">
									<span className="thead-mob">Reward APY</span>
									<span className="percent">
										<span>+7.72%</span>
										<img src={ arrow_icon } alt="arrow" />
										<span>+7.72%</span>
									</span>
									<span className="title">Curve</span>
								</td>
								<td className="pool-vl"> <span className="thead-mob">Volume</span>$62,3m</td>
							</tr>
							<tr>
								<td className="pool-pl">
									<span className="icons">
										<img src={ dai_icon } alt="DAI" />
										<img src={ wbtc_icon } alt="WBTC" />
										<img src={ usdc_icon } alt="USDC" />
									</span>
									<span className="heading">3 Pools<i>USD</i><span>DAI + USDC + USDT</span></span>
								</td>
								<td className="pool-rew">
									<span className="thead-mob">Reward APY</span>
									<span className="percent">
										<span>+7.72%</span>
										<img src={ arrow_icon } alt="arrow" />
										<span>+7.72%</span>
									</span>
									<span className="title">Curve</span>
								</td>
								<td className="pool-vl">
									<span className="thead-mob">Volume</span>$62,3m
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		)

	}
}

export default PoolsPage;