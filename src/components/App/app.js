
import React from 'react';

import { createStore } from 'redux';

import {
	reducer,
	metamaskConnectionRequest,
} from '../../reducers';

import MetamaskAdapter from '../../models/MetamaskAdapter';

import Header       from '../Header';
import DepositPage  from '../DepositPage';
import ExchangePage from '../ExchangePage';
import WithdrawPage from '../WithdrawPage';
import MessagePopup from '../MessagePopup';

import metamask_logo  from '../../static/pics/logo-metamask.svg'
import attention_icon from '../../static/pics/attention.svg'

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			currentPage         : '',
			metamaskLogged      : false,
			metamaskNotInstalled: false,
			permissionRejected  : false,
			wrongChain          : false,
			address             : '',
			balanceETH          : 0,
			balanceNIFTSY       : 0,
			transferNIFTSY_value: '',
			transferNIFTSY_recipient: '',
			transfer_status: '',

			recievedEvents: [],
		};
		this.store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

		this.metamaskAdapter = new MetamaskAdapter({ store: this.store });
	};

	componentWillMount() {
		this.unsubscribe = this.store.subscribe(() => {
			this.setState({
				currentPage         : this.store.getState().currentPage,

				metamaskNotInstalled: this.store.getState().metamaskAdapter.metamaskNotInstalled,
				permissionRejected  : this.store.getState().metamaskAdapter.permissionRejected,
				metamaskLogged      : this.store.getState().metamaskAdapter.logged,
				wrongChain          : this.store.getState().metamaskAdapter.wrongChain,
				address             : this.store.getState().account.address,
				balanceETH          : this.store.getState().account.balanceETH,
				balanceNIFTSY       : this.store.getState().account.balanceNIFTSY,
				transfer_status     : this.store.getState().operations._msg,
			});
		});

		this.store.dispatch(metamaskConnectionRequest());
 	}

 	componentWillUnmount() {
 		this.unsubscribe();
 	}

 	getCurrentPage() {
 		if (this.state.currentPage === 'deposit') {
 			return <DepositPage store = { this.store } />
 		}
 		if (this.state.currentPage === 'exchange') {
 			return <ExchangePage store = { this.store } />
 		}
 		if (this.state.currentPage === 'withdraw') {
 			return <WithdrawPage store = { this.store } />
 		}
 	}

 	getMessagePopup() {
 		if ( this.store.getState()._messagePopup ) {
 			return <MessagePopup store = {this.store} />
 		} else {
 			return ''
 		}
 	}

 	getMetamaskWarning() {
 		if (this.state.metamaskNotInstalled) {
			return (
				<div class="modal" id="modal-1">
				<div class="modal__inner">
				<div class="modal__bg"></div>
				<div class="container">
				<div className="modal__content">
					<div className="mb-text">
						<img className="w-logo" src={ metamask_logo } alt="Attention" />
						<h2>Install metamask</h2>
						<p>Install Metamask extension to get access to service</p>
					</div>
				</div>
				</div>
				</div>
				</div>
			)
		}
		if (this.state.permissionRejected) {
			return (
				<div class="modal" id="modal-1">
				<div class="modal__inner">
				<div class="modal__bg"></div>
				<div class="container">
				<div className="modal__content">
					<div className="mb-text">
						<img className="w-logo" src={ metamask_logo } alt="Attention" />
						<h2>Metamask access</h2>
						<p>You should grant access to you MetaMask Wallet</p>
					</div>
					<div className="mb-btns">
						<button
							className="btn btn-big btn-active"
							onClick={()=>{ this.store.dispatch(metamaskConnectionRequest()); }}
						>LOGIN</button>
					</div>
				</div>
				</div>
				</div>
				</div>
			)
		}
		if (!this.state.metamaskLogged) {
			return (
				<div class="modal" id="modal-1">
				<div class="modal__inner">
				<div class="modal__bg"></div>
				<div class="container">
				<div className="modal__content">
					<div className="mb-text">
						<img className="w-logo" src={ metamask_logo } alt="Attention" />
						<h2>Connect Wallet</h2>
						<p>Use Metamask to&nbsp;connect your personal account and&nbsp;use the Curve functionality</p>
					</div>
					<div className="mb-btns">
						<button
							className="btn btn-big btn-active"
							onClick={()=>{ this.store.dispatch(metamaskConnectionRequest()); }}
						>LOGIN</button>
					</div>
				</div>
				</div>
				</div>
				</div>
			)
		}
		if (this.state.wrongChain) {
			return (
				<div class="modal" id="modal-1">
				<div class="modal__inner">
				<div class="modal__bg"></div>
				<div class="container">
				<div className="modal__content">
					<div className="mb-text">
						<img className="w-logo" src={ attention_icon } alt="Attention" />
						<h2>Wrong network</h2>
						<p>Select Rinkeby network and login again</p>
					</div>
					<div className="mb-btns">
						<button
							className="btn btn-big btn-active"
							onClick={()=>{ this.store.dispatch(metamaskConnectionRequest()); }}
						>LOGIN</button>
					</div>
				</div>
				</div>
				</div>
				</div>
			)
		}
 	}

	render() {

		return (
			<React.Fragment>
			<Header store = { this.store } />

			<div className="container">
				<div className="content">
					{ this.getCurrentPage() }
				</div>

				{ this.getMetamaskWarning() }

			</div>

			</React.Fragment>
		)



	}

}

export default App;