import React from 'react';

import {
	goToDeposit,
	goToExchange,
	goToWithdraw,
	metamaskConnectionRequest,
} from '../../reducers'

class Header extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			currentPage   : this.props.store.getState().currentPage,
			metamaskLogged: this.props.store.getState().metamaskAdapter.logged,
		};
	}

	componentDidMount() {
		this.unsubscribe = this.props.store.subscribe(() => {
			this.setState({
				currentPage   : this.props.store.getState().currentPage,
				metamaskLogged: this.props.store.getState().metamaskAdapter.logged,
			});
		})
	}
	componentWillUnmount() {
		this.unsubscribe();
	}

	getConnectBtn() {
		if (this.state.metamaskLogged) {
			return (
				<button
					className="btn btn-big btn-active"
					disabled={true}
				>Wallet Connected</button>
			)
		} else {
			return (
				<button
					className="btn btn-big btn-active"
					onClick={()=>{ this.store.dispatch(metamaskConnectionRequest()); }}
				>Connect Wallet</button>)
		}
	}


	render() {

		return (
			<header className="s-header">
				<div className="container">

					<a className="s-header__logo" href=""><img src="../pics/logo.svg" alt="" /></a>

					<div className="s-header__toggle"><span></span><span> </span><span></span></div>

					<div className="s-header__nav">
						<div className="s-header__menu">
							<a
								onClick={() => { this.props.store.dispatch(goToDeposit())  }}
								className={ this.state.currentPage === 'deposit' ? 'active' : '' }
							>Deposit</a>
							<a
								onClick={() => { this.props.store.dispatch(goToExchange()) }}
								className={ this.state.currentPage === 'exchange' ? 'active' : '' }
							>Buy and Sell</a>
							<a
								onClick={() => { this.props.store.dispatch(goToWithdraw()) }}
								className={ this.state.currentPage === 'withdraw' ? 'active' : '' }
							>Withdraw</a>
						</div>
						{ this.getConnectBtn() }
					</div>

				</div>
			</header>
		)

	}
}

export default Header;