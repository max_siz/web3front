
import Web3 from 'web3';

import {
	metamaskConnectionStarted,
	metamaskConnectionNotInstalled,
	metamaskConnectionSuccess,
	metamaskConnectionRejected,
	metamaskConnectionRequest,

	accountUpdateBalanceRequest,
	accountUpdateBalanceStarted,
	accountUpdateBalanceSuccess,

	transferStarted,
	transferSuccess,
} from '../../reducers'

export default class MetamaskAdpter {

	constructor(props) {
		this.props = props;

		this.unsubscribe = this.props.store.subscribe(() => {

			if (this.props.store.getState().metamaskAdapter.metamaskConnectionRequest) {
				this.props.store.dispatch(metamaskConnectionStarted());
				this.connectMetamask();
			}

			if (this.props.store.getState().account.balanceRequest) {
				this.props.store.dispatch(accountUpdateBalanceStarted());
				this.getBalance();
			}

			if (this.props.store.getState().operations.transferNIFTSY_value) {
				this.transferNIFTSY(
					this.props.store.getState().operations.transferNIFTSY_recipient,
					this.props.store.getState().operations.transferNIFTSY_value
				);
				this.props.store.dispatch(transferStarted());
			}

		});
		this.props.store.dispatch(metamaskConnectionRequest());
	}

	createNIFTSYContract() {
		const jsonInterface = JSON.parse('[{"inputs":[{"internalType":"string","name":"name_","type":"string"},{"internalType":"string","name":"symbol_","type":"string"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"}]');
		this.niftsyContract = new this.web3.eth.Contract(jsonInterface, '0x27E0fF339A2053Ee31093026571f4037419E5c63');
	}

	connectMetamask() {
		if (window.ethereum) {
			this.web3 = new Web3(window.ethereum);

			window.ethereum.enable()
				.then(() => {
					this.createNIFTSYContract();

					this.props.store.dispatch(metamaskConnectionSuccess({
						address: this.web3.givenProvider.selectedAddress,
					}));
					this.props.store.dispatch(accountUpdateBalanceRequest());
				})
				.catch((e) => {
					console.log(`Cannot connect to metamask: ${e.toString()}`);
					this.props.store.dispatch(metamaskConnectionRejected());
				})

		} else if (window.web3) {
			this.web3 = new Web3(window.web3.currentProvider);
		} else {
			this.props.store.dispatch(metamaskConnectionNotInstalled());
		}
	}

	async getBalance() {
		if (!this.props.store.getState().account.address) { return; }

		const balanceETH = await this.web3.eth.getBalance(this.props.store.getState().account.address);

		const balanceNIFTSY = await this.niftsyContract.methods.balanceOf( this.props.store.getState().account.address ).call();

		this.props.store.dispatch(accountUpdateBalanceSuccess({ balanceETH, balanceNIFTSY }));
	}

	async transferNIFTSY(recipient, value) {
		const transferResult = await this.niftsyContract.methods.transfer( recipient, value ).send({ from: this.props.store.getState().account.address });
		this.props.store.dispatch(transferSuccess({ msg: JSON.stringify(transferResult, '', 4) }));
		this.props.store.dispatch(accountUpdateBalanceRequest());
	}

}