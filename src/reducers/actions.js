// ---------- NAVIGATION ----------
export const goToExchange = () => {
	return {
		type: 'GOTO_EXCHANGE',
	}
}
export const goToDeposit = () => {
	return {
		type: 'GOTO_DEPOSIT',
	}
}
export const goToWithdraw = () => {
	return {
		type: 'GOTO_WITHDRAW',
	}
}
export const showMessage = (payload) => {
	return {
		type: 'SHOW_MESSAGE',
		payload,
	}
}
export const hideMessage = () => {
	return {
		type: 'HIDE_MESSAGE',
	}
}
// ---------- END NAVIGATION ----------

// ---------- CONNECTION ----------
export const metamaskConnectionRequest = () => {
	return {
		type: 'METAMASK_CONNECTION_REQUEST',
	}
}
export const metamaskConnectionStarted = () => {
	return {
		type: 'METAMASK_CONNECTION_STARTED',
	}
}
export const metamaskConnectionSuccess = (payload) => {
	return {
		type: 'METAMASK_CONNECTION_SUCCESS',
		payload,
	}
}
export const metamaskConnectionNotInstalled = () => {
	return {
		type: 'METAMASK_CONNECTION_NOT_INSTALLED',
	}
}
export const metamaskConnectionRejected = () => {
	return {
		type: 'METAMASK_CONNECTION_REJECTED',
	}
}
// ---------- END CONNECTION ----------

// ---------- ACCOUNT ----------
export const accountUpdateBalanceRequest = () => {
	return {
		type: 'ACCOUNT_UPDATE_BALANCE_REQUEST',
	}
}
export const accountUpdateBalanceStarted = () => {
	return {
		type: 'ACCOUNT_UPDATE_BALANCE_STARTED',
	}
}
export const accountUpdateBalanceSuccess = (payload) => {
	return {
		type: 'ACCOUNT_UPDATE_BALANCE_SUCCESS',
		payload,
	}
}
export const accountUpdateBalanceError = (payload) => {
	return {
		type: 'ACCOUNT_UPDATE_BALANCE_ERROR',
		payload,
	}
}
// ---------- END ACCOUNT ----------

// ---------- OPERATIONS ----------
export const transferRequest = (payload) => {
	return {
		type: 'TRANSFER_REQUEST',
		payload,
	}
}
export const transferStarted = () => {
	return {
		type: 'TRANSFER_STARTED',
	}
}
export const transferSuccess = (payload) => {
	return {
		type: 'TRANSFER_SUCCESS',
		payload,
	}
}
export const transferError = (payload) => {
	return {
		type: 'TRANSFER_ERROR',
		payload,
	}
}
// ---------- END OPERATIONS ----------