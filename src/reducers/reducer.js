export const initialState = {

	currentPage: 'exchange',
	_messagePopup: '',

	metamaskAdapter: {
		logged: false,
		metamaskNotInstalled: false,
		permissionRejected: false,
		metamaskConnectionRequest: false,
		wrongChain: false,
	},
	account: {
		_error: '',
		balanceRequest: false,
		addressRequest: false,
		address: '',
		balanceETH: 0,
		balanceNIFTSY: 0,
	},
	operations: {
		_error: '',
		_msg: '',
		transferNIFTSY_value: 0,
		transferNIFTSY_recipient: '',
	},

}

export const reducer = (state = initialState, action) => {

	switch ( action.type ) {

		// ---------- NAVIGATION ----------
		case 'GOTO_EXCHANGE': {
			return {
				...state,
				currentPage: 'exchange',
			}
		}
		case 'GOTO_DEPOSIT': {
			return {
				...state,
				currentPage: 'deposit',
			}
		}
		case 'GOTO_WITHDRAW': {
			return {
				...state,
				currentPage: 'withdraw',
			}
		}
		case 'SHOW_MESSAGE': {
			return {
				...state,
				_messagePopup: action.payload.messageHTML,
			}
		}
		case 'HIDE_MESSAGE': {
			return {
				...state,
				_messagePopup: '',
			}
		}
		// ---------- END NAVIGATION ----------

		// ---------- CONNECTION ----------
		case 'METAMASK_CONNECTION_REQUEST': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					metamaskConnectionRequest: true,
				}
			}
		}
		case 'METAMASK_CONNECTION_STARTED': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					metamaskConnectionRequest: false,
				}
			}
		}
		case 'METAMASK_CONNECTION_SUCCESS': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					metamaskNotInstalled: false,
					permissionRejected: false,
					logged: true,
				},
				account: {
					address: action.payload.address,
				}
			}
		}
		case 'METAMASK_CONNECTION_NOT_INSTALLED': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					metamaskNotInstalled: true,
				}
			}
		}
		case 'METAMASK_CONNECTION_REJECTED': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					permissionRejected: true,
				}
			}
		}
		case 'METAMASK_CONNECTION_WRONG_CHAIN': {
			return {
				...state,
				metamaskAdapter: {
					...state.metamaskAdapter,
					wrongChain: true,
				}
			}
		}
		// ---------- END CONNECTION ----------

		// ---------- ACCOUNT ----------
		case 'ACCOUNT_UPDATE_BALANCE_REQUEST': {
			return {
				...state,
				account: {
					...state.account,
					balanceRequest: true,
				}
			}
		}
		case 'ACCOUNT_UPDATE_BALANCE_STARTED': {
			return {
				...state,
				account: {
					...state.account,
					balanceRequest: false,
				}
			}
		}
		case 'ACCOUNT_UPDATE_BALANCE_SUCCESS': {
			return {
				...state,
				account: {
					...state.account,
					balanceETH: action.payload.balanceETH,
					balanceNIFTSY: action.payload.balanceNIFTSY,
				}
			}
		}
		case 'ACCOUNT_UPDATE_BALANCE_ERROR': {
			return {
				...state,
				account: {
					...state.account,
					_error: action.payload.error,
				}
			}
		}
		// ---------- END ACCOUNT ----------

		// ---------- OPERATIONS ----------
		case 'TRANSFER_REQUEST': {
			return {
				...state,
				operations: {
					...state.operations,
					transferNIFTSY_value: action.payload.value,
					transferNIFTSY_recipient: action.payload.recipient,
				}
			}
		}
		case 'TRANSFER_STARTED': {
			return {
				...state,
				operations: {
					...state.operations,
					transferNIFTSY_value: 0,
					transferNIFTSY_recipient: '',
					_msg: 'Waiting...',
				}
			}
		}
		case 'TRANSFER_SUCCESS': {
			return {
				...state,
				operations: {
					...state.operations,
					_msg: action.payload.msg,
				}
			}
		}
		case 'TRANSFER_ERROR': {
			return {
				...state,
				operations: {
					...state.operations,
					_error: action.payload.error,
				}
			}
		}
		// ---------- END OPERATIONS ----------

		default: { return state }

	}

}